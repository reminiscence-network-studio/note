## windows系统下调试ios safari h5移动端项目

大家好，我是小朱，今天再来给大家推荐一个windows系统 下使用edge浏览器 调试 ios h5移动端项目的技巧。这里不推荐chrome浏览器，因为国墙的原因，使用chrome浏览器连接手机不太稳定会经常白屏。

&gt; **安装调试环境**

废话不多说，先来安装调试环境，首先需要安装 `scoop` ,scoop是一个包管理工具，而安装 `scoop` 需要电脑里有powershell。win10以上系统都自带了，如果你没有可以去下载一个powershell 5.1或以上版本。

```powershell
#修改执行策略，选择是
set-executionpolicy unrestricted -s cu 

#安装scoop
iex (new-object net.webclient).downloadstring('https://get.scoop.sh') 
```

&gt; **通过scoop安装ios_webkit_debug_proxy**

//先来添加一个镜像源

```powershell
scoop bucket add extras // 安装 bucket
scoop install ios-webkit-debug-proxy // 安装 ios-webkit-debug-proxy
```

查看是否安装成功：**scoop list**

&gt; **安装最新版本的 *remotedebug-ios-webkit-adapter***

```powershell
npm install -g vs-libimobile
npm install remotedebug-ios-webkit-adapter -g
```

```powershell
remotedebug_ios_webkit_adapter --port=9000
```

启用开发者模式进行调试。

*Iphone =&gt; 设置 =&gt; Safari 浏览器 =&gt; 高级 =&gt; web检查器 =&gt; 启用*

&gt; **连接测试~~连接方式(原理一样，启动代理端口)**

```powershell
remotedebug_ios_webkit_adapter --port=9000
```

打开edge调试工具

edge://inspect/#devices